# Translating Bike Images into van Gogh Artwork Using a Generative Adverserial Network (GAN)

* Implements CycleGAN, a Cycle-Consistent Adversarial Network by [Zhu et al. (2017)](https://arxiv.org/abs/1703.10593)
* Written in Python via a Jupyter Notebook
  - Uses TensorFlow
* Utilizes WikiArt Dataset ([wikiart.org](http://wikiart.org))